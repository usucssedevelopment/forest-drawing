﻿using System.IO;

namespace AppLayer.Command
{
    public class SaveCommand : Command
    {
        private readonly string _filename;
        internal SaveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                _filename = commandParameters[0] as string;
        }

        public override void Execute()
        {
            if (TargetDrawing == null) return;

            var targetFilename = _filename;
            if (string.IsNullOrWhiteSpace(targetFilename))
                targetFilename = TargetDrawing.Filename;

            if (string.IsNullOrWhiteSpace(targetFilename))
                return;

            var writer = new StreamWriter(targetFilename);
            TargetDrawing?.SaveToStream(writer.BaseStream);
            writer.Close();
        }
    }
}
