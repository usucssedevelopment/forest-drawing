﻿namespace AppLayer.DrawingComponents
{
    public class AnotherBoardleafTree : Tree
    {
        public static string Name { get; } = "Another Broadleaf";

        public override string TreeName => Name;

        public override string ResourceName => "Another Boardleaf Tree.png";
    }
}
