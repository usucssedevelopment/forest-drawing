﻿namespace AppLayer.DrawingComponents
{
    public class PineTree : Tree
    {
        public static string Name { get; } = "Pine";

        public bool HasPineCones { get; set; }
        public override string TreeName => Name;

        public override string ResourceName => "Pine Tree.png";
    }
}
