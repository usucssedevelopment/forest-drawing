﻿namespace AppLayer.DrawingComponents
{
    public class OakTree : Tree
    {
        public static string Name { get; } = "Oak";

        public int NumberOfSquirrels { get; set; }
        public override string TreeName => Name;

        public override string ResourceName => "Oak Tree.png";
    }
}
