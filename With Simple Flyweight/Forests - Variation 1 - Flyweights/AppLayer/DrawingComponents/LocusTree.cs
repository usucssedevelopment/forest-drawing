﻿namespace AppLayer.DrawingComponents
{
    public class LocusTree : Tree
    {
        public static string Name { get; } = "Locus";

        public bool HasSwing { get; set; }

        public override string ResourceName => "Locus Tree.png";
    }
}
