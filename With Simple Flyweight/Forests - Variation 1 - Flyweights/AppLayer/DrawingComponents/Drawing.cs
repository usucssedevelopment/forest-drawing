﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;

namespace AppLayer.DrawingComponents
{
    public class Drawing
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<TreeExtrinsicState>));

        private readonly List<TreePlacement> _treePlacements = new List<TreePlacement>();
        private readonly object _myLock = new object();
        public bool IsDirty { get; private set; } = true;

        public TreeFactory TreeFactory { get; set; }


        public bool Render(Graphics graphics, bool redrawEvenIfNotDirty = false)
        {
            lock (_myLock)
            {
                if (!IsDirty && !redrawEvenIfNotDirty) return false;
                graphics.Clear(Color.White);
                foreach (var placement in _treePlacements)
                    placement.Tree.Render(graphics, placement.ExtrinsicState);
                IsDirty = false;
            }
            return true;
        }

        public void Clear()
        {
            lock (_myLock)
            {
                _treePlacements.Clear();
                IsDirty = true;
            }
        }

        public void Load(string filename)
        {
            var reader = new StreamReader(filename);
            var extrinsicStates = JsonSerializer.ReadObject(reader.BaseStream) as List<TreeExtrinsicState>;
            if (extrinsicStates == null) return;

            lock (_myLock)
            {
                _treePlacements.Clear();
                foreach (var extrinsicState in extrinsicStates)
                {
                    var tree = TreeFactory.CreateTree(extrinsicState.TreeType);
                    _treePlacements.Add(new TreePlacement(tree, extrinsicState));
                }
                IsDirty = true;
            }
            reader.Close();
        }

        public void Save(string filename)
        {
            var writer = new StreamWriter(filename);
            var extrinsicStates = new List<TreeExtrinsicState>();
            lock (_myLock)
            {
                extrinsicStates.AddRange(_treePlacements.Select(tree => tree.ExtrinsicState));
            }
            JsonSerializer.WriteObject(writer.BaseStream, extrinsicStates);
            writer.Close();
        }

        public void Add(TreePlacement treePlacement)
        {
            if (treePlacement == null) return;
            lock (_myLock)
            {
                _treePlacements.Add(treePlacement);
                IsDirty = true;
            }
        }

        public TreePlacement FindTreeAtPosition(Point location)
        {
            TreePlacement result;
            lock (_myLock)
            {
                result = _treePlacements.FindLast(placement => location.X >= placement.ExtrinsicState.Location.X &&
                                              location.X < placement.ExtrinsicState.Location.X + placement.ExtrinsicState.Size.Width &&
                                              location.Y >= placement.ExtrinsicState.Location.Y &&
                                              location.Y < placement.ExtrinsicState.Location.Y + placement.ExtrinsicState.Size.Height);
            }
            return result;
        }

        public void ToggleSelectionAtPosition(Point location)
        {
            var tree = FindTreeAtPosition(location);

            if (tree!=null)
                tree.ExtrinsicState.IsSelected = !tree.ExtrinsicState.IsSelected;

            IsDirty = true;
        }

        public void DeleteAllSelected()
        {
            lock (_myLock)
            {
                _treePlacements.RemoveAll(t => t.ExtrinsicState.IsSelected);
                IsDirty = true;
            }
        }

        public void DeselectAll()
        {
            lock (_myLock)
            {
                foreach (var t in _treePlacements)
                    t.ExtrinsicState.IsSelected = false;
                IsDirty = true;
            }
        }

    }
}
