﻿namespace AppLayer.DrawingComponents
{
    public class BirchTree : Tree
    {
        public static string Name { get; } = "Birch";

        public override string ResourceName => "Birch Tree.png";
    }
}
