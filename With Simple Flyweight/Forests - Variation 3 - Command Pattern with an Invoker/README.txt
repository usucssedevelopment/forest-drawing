This variation of the Forest Drawing programs makes the following changes to the first variation:

	- Redecude coupling of GUI from drawing and other drawing components.
	
The program only supports only one active diagram at a time.
