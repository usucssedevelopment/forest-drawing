﻿namespace AppLayer.DrawingComponents
{
    public class TreePlacement
    {
        public Tree Tree { get; set; }
        public TreeExtrinsicState ExtrinsicState { get; set; }
        public TreePlacement(Tree tree, TreeExtrinsicState state)
        {
            Tree = tree;
            ExtrinsicState = state;
        }
    }
}
