﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;

namespace AppLayer.DrawingComponents
{
    // NOTE: This class at least one  problem that is hurting coupling and lowering cohesion

    public class Drawing
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<TreeExtrinsicState>));

        private readonly List<TreePlacement> _treePlacements = new List<TreePlacement>();
        private readonly object _myLock = new object();

        protected bool IsDirty { get; private set; } = true;

        public TreeFactory TreeFactory { get; set; }
        public string Filename { get; set; }

        public bool Draw(Graphics graphics, bool redrawEvenIfNotDirty = false)
        {
            lock (_myLock)
            {
                if (!IsDirty && !redrawEvenIfNotDirty) return false;
                graphics.Clear(Color.White);
                foreach (var placement in _treePlacements)
                    placement.Tree.Render(graphics, placement.ExtrinsicState);
                IsDirty = false;
            }
            return true;
        }

        internal void Clear()
        {
            lock (_myLock)
            {
                _treePlacements.Clear();
                IsDirty = true;
            }
        }

        internal void LoadFromStream(Stream stream)
        {
            var extrinsicStates = JsonSerializer.ReadObject(stream) as List<TreeExtrinsicState>;
            if (extrinsicStates == null) return;

            lock (_myLock)
            {
                _treePlacements.Clear();
                foreach (var extrinsicState in extrinsicStates)
                {
                    var tree = TreeFactory.CreateTree(extrinsicState.TreeType);
                    _treePlacements.Add(new TreePlacement(tree, extrinsicState));
                }
                IsDirty = true;
            }

        }

        internal void SaveToStream(Stream stream)
        {
            var extrinsicStates = new List<TreeExtrinsicState>();
            lock (_myLock)
            {
                extrinsicStates.AddRange(_treePlacements.Select(tree => tree.ExtrinsicState));
            }
            JsonSerializer.WriteObject(stream, extrinsicStates);
        }

        internal void Add(TreePlacement treePlacement)
        {
            if (treePlacement == null) return;
            lock (_myLock)
            {
                _treePlacements.Add(treePlacement);
                IsDirty = true;
            }
        }

        internal TreePlacement FindTreeAtPosition(Point location)
        {
            TreePlacement result;
            lock (_myLock)
            {
                result = _treePlacements.FindLast(placement => location.X >= placement.ExtrinsicState.Location.X &&
                                              location.X < placement.ExtrinsicState.Location.X + placement.ExtrinsicState.Size.Width &&
                                              location.Y >= placement.ExtrinsicState.Location.Y &&
                                              location.Y < placement.ExtrinsicState.Location.Y + placement.ExtrinsicState.Size.Height);
            }
            return result;
        }


        internal void ToggleSelectionAtPosition(Point location)
        {
            var tree = FindTreeAtPosition(location);

            if (tree != null)
                tree.ExtrinsicState.IsSelected = !tree.ExtrinsicState.IsSelected;

            IsDirty = true;
        }

        internal void DeleteAllSelected()
        {
            lock (_myLock)
            {
                _treePlacements.RemoveAll(t => t.ExtrinsicState.IsSelected);
                IsDirty = true;
            }
        }

        internal void DeselectAll()
        {
            lock (_myLock)
            {
                foreach (var t in _treePlacements)
                    t.ExtrinsicState.IsSelected = false;
                IsDirty = true;
            }
        }

    }
}
