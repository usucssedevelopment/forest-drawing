﻿using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    /// <summary>
    /// Base Command Class
    /// 
    /// This is the abstract Command class prescribed by the command pattern
    /// </summary>
    public abstract class Command
    {
        public TreeFactory Factory { get; set; }
        public Drawing TargetDrawing { get; set; }      // "Receiver" in the Command Pattern

        public abstract void Execute();
    }
}
