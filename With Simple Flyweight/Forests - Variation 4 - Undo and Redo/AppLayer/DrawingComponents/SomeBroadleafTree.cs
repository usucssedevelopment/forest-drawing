﻿namespace AppLayer.DrawingComponents
{
    public class SomeBroadleafTree : Tree
    {
        public static string Name { get; } = "Some Broadleaf";

        public int NumberOfBirds { get; set; }
        public override string TreeName => Name;

        public override string ResourceName => "Some Broadleaf Tree.png";
    }
}
