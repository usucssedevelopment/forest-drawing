﻿namespace AppLayer.DrawingComponents
{
    public class AshTree : Tree
    {
        public static string Name { get; } = "Ash";

        public override string TreeName => Name;
        public override string ResourceName => "Ash Tree.png";

    }
}
