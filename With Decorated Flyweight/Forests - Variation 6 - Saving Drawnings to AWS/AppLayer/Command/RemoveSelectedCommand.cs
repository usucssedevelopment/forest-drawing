﻿using System.Collections.Generic;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class RemoveSelectedCommand : Command
    {
        private List<Tree> _deletedTrees;
        internal RemoveSelectedCommand() { }

        internal override bool Execute()
        {
            if (!IsValid) return false;

            _deletedTrees = TargetDrawing.Trees(t => t.IsSelected);
            return TargetDrawing.DeleteAllSelected();
        }

        internal override bool Undo()
        {
            if (_deletedTrees == null || _deletedTrees.Count==0) return false;

            foreach (Tree t in _deletedTrees)
                TargetDrawing.Add(t);

            return true;
        }
    }
}
