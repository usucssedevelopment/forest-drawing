﻿using System.Drawing;

namespace AppLayer.Command
{
    public class SelectCommand : Command
    {
        private readonly Point _location;

        internal SelectCommand(params object[] commandParameters)
        {
            if (commandParameters.Length>0)
            _location = (Point) commandParameters[0];
        }

        internal override bool Execute()
        {
            if (!IsValid) return false;

            return TargetDrawing?.Select(_location) ?? false;
        }
        internal override bool Undo()
        {
            return TargetDrawing?.Select(_location) ?? false;
        }
    }
}
