﻿using System;
using System.Collections.Generic;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class NewCommand : Command
    {
        private List<Tree> _allTrees;

        internal NewCommand() {}

        internal override bool Execute()
        {
            if (!IsValid) return false;

            _allTrees = TargetDrawing.Trees();

            TargetDrawing?.Clear();
            return true;
        }

        internal override bool Undo()
        {
            if (_allTrees == null || _allTrees.Count == 0) return false;

            foreach(Tree t in _allTrees)
                TargetDrawing.Add(t);

            return true;
        }
    }
}
