﻿using System;
using System.IO;

namespace AppLayer.Command
{
    public class LoadCommand : Command
    {
        private readonly string _bucket;
        private readonly string _fileOrKey;
        private readonly bool? _loadToAws;

        internal LoadCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                _bucket = commandParameters[0] as string;

            if (commandParameters.Length > 1)
                _fileOrKey = commandParameters[1] as string;

            if (commandParameters.Length > 2)
                _loadToAws = (bool) commandParameters[2];
        }

        internal override bool IsValid => base.IsValid &&
            (_loadToAws!=true || !string.IsNullOrWhiteSpace(_bucket)) &&
            !string.IsNullOrWhiteSpace(_fileOrKey) &&
            _loadToAws != null;

        internal override bool Execute()
        {
            if (!IsValid) return false;

            return _loadToAws ==true ? LoadFromAws() : LoadFromFile();
        }

        private bool LoadFromFile()
        {
            bool result = true;
            try
            {
                StreamReader reader = new StreamReader(_fileOrKey);
                TargetDrawing?.LoadFromStream(reader.BaseStream);
                reader.Close();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private bool LoadFromAws()
        {
            bool result = false;

            // TODO

            return result;
        }

        internal override bool Undo()
        {
            TargetDrawing.Clear();
            return true;
        }
    }
}
