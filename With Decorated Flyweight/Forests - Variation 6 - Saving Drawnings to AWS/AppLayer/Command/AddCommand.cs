﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class AddCommand : Command
    {
        private const int NormalWidth = 80;
        private const int NormalHeight = 80;

        private readonly string _treeType;
        private Point? _location;
        private readonly float? _scale;

        private Tree _tree;

        internal AddCommand() { }

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="commandParameters">An array of parameters, where
        ///     [1]: string     tree type -- a fully qualified resource name
        ///     [2]: Point      center location for the tree, defaut = top left corner
        ///     [3]: float      scale factor</param>
        internal AddCommand(params object[] commandParameters)
        {
            if (commandParameters.Length>0)
                _treeType = commandParameters[0] as string;

            if (commandParameters.Length > 1)
                _location = (Point) commandParameters[1];
            else
                _location = new Point(0, 0);

            if (commandParameters.Length > 2)
                _scale = (float) commandParameters[2];
            else
                _scale = 1.0F;
        }

        internal override bool IsValid => base.IsValid && _treeType != null && _location != null && _scale != null;

        internal override bool Execute()
        {
            if (!IsValid) return false;

            if (_tree == null)
            {
                Size treeSize = new Size()
                {
                    Width = Convert.ToInt16(Math.Round(NormalWidth*_scale.Value, 0)),
                    Height = Convert.ToInt16(Math.Round(NormalHeight*_scale.Value, 0))
                };
                Point treeLocation = new Point(_location.Value.X - treeSize.Width/2, _location.Value.Y - treeSize.Height/2);

                TreeExtrinsicState extrinsicState = new TreeExtrinsicState()
                {
                    TreeType = _treeType,
                    Location = treeLocation,
                    Size = treeSize
                };
                _tree = TargetDrawing.Factory.GetTree(extrinsicState);
            }
            TargetDrawing.Add(_tree);

            return true;
        }

        internal override bool Undo()
        {
            return TargetDrawing.RemoveTree(_tree);
        }
    }
}
