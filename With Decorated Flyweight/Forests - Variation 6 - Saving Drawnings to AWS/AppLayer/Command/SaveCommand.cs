﻿using System;
using System.IO;

namespace AppLayer.Command
{
    public class SaveCommand : Command
    {
        private readonly string _bucket;
        private readonly string _fileOrKey;
        private readonly bool? _saveToAws;


        internal SaveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                _bucket = commandParameters[0] as string;

            if (commandParameters.Length > 1)
                _fileOrKey = commandParameters[1] as string;

            if (commandParameters.Length > 2)
                _saveToAws = (bool)commandParameters[2];
        }

        internal override bool IsValid => base.IsValid &&
                (_saveToAws != true || !string.IsNullOrWhiteSpace(_bucket)) &&
                !string.IsNullOrWhiteSpace(_fileOrKey) &&
                _saveToAws != null;

        internal override bool Execute()
        {
            if (!IsValid) return false;

            return _saveToAws == true ? SaveToAws() : SaveToFile();
        }

        private bool SaveToFile()
        {
            bool result = true;
            try
            {
                StreamWriter writer = new StreamWriter(_fileOrKey);
                TargetDrawing?.SaveToStream(writer.BaseStream);
                writer.Close();
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private bool SaveToAws()
        {
            bool result = false;

            // TODO

            return result;
        }

        internal override bool Undo()
        {
            // Can't undo a save command
            return false;
        }
    }
}
