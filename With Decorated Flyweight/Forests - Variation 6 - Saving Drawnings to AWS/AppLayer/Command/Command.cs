﻿using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    /// <summary>
    /// Base Command Class
    /// 
    /// This is the abstract Command class prescribed by the command pattern
    /// </summary>
    public abstract class Command
    {
        public Drawing TargetDrawing { get; set; }      // "Receiver" in the Command Pattern

        internal virtual bool IsValid => TargetDrawing != null;
        internal abstract bool Execute();
        internal abstract bool Undo();
    }
}
