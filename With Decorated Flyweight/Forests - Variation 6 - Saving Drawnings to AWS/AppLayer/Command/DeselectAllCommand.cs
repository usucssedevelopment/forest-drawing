﻿using System.Collections.Generic;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class DeselectAllCommand : Command
    {
        private List<Tree> _previouslySelectedTree;
        internal DeselectAllCommand() { }

        internal override bool Execute()
        {
            if (!IsValid) return false;

            _previouslySelectedTree = TargetDrawing.Trees(t => t.IsSelected);
            return TargetDrawing?.DeselectAll() ?? false;
        }

        internal override bool Undo()
        {
            return TargetDrawing?.SelectAll(_previouslySelectedTree) ?? false;
        }
    }
}
