﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AppLayer.Command;
using AppLayer.DrawingComponents;

namespace Forests
{
    // NOTE: There some design problems with this class

    public partial class MainForm : Form
    {
        private readonly Drawing _drawing;
        private readonly CommandFactory _commandFactory;
        private readonly Invoker _invoker = new Invoker();
        private string _currentTreeResource;
        private float _currentScale = 1;

        private readonly Command _newCommand;
        private readonly Command _deselectAllCommand;
        private readonly Command _removedSelectedCommand;
        private string _loadAndSaveResponse;
        private bool _loadAndSaveUseAws;

        private enum PossibleModes
        {
            None,
            TreeDrawing,
            Selection
        };

        private PossibleModes _mode = PossibleModes.None;

        private Bitmap _imageBuffer;
        private Graphics _imageBufferGraphics;
        private Graphics _panelGraphics;
       
        public MainForm()
        {
            InitializeComponent();

            _drawing = new Drawing();           
            _commandFactory = new CommandFactory() {TargetDrawing = _drawing};
            _drawing.Factory = new TreeFactory()
            {
                ResourceNamePattern = @"Forests.Graphics.{0}.png",
                ReferenceType = typeof(Program)
            };
            _invoker.Start();

            _newCommand = _commandFactory.Create("new");
            _deselectAllCommand = _commandFactory.Create("deselect");
            _removedSelectedCommand = _commandFactory.Create("Remove");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
            refreshTimer.Start();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _invoker.Stop();
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            DisplayDrawing();
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            _invoker.EnqueueCommandForExecution(_newCommand);
        }

        private void ClearOtherSelectedTools(ToolStripButton ignoreItem)
        {
            foreach (ToolStripItem item in drawingToolStrip.Items)
            {
                ToolStripButton toolButton = item as ToolStripButton;
                if (toolButton != null && item != ignoreItem && toolButton.Checked)
                {
                    toolButton.Checked = false;
                }
            }
        }

        private void pointerButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            ClearOtherSelectedTools(button);

            if (button!=null && button.Checked)
            {
                _mode = PossibleModes.Selection;
                _currentTreeResource = string.Empty;
            }
            else
            {
                _invoker.EnqueueCommandForExecution(_deselectAllCommand);
                _mode = PossibleModes.None;
            }
        }

        private void treeButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
            {
                _currentTreeResource = button.Text;
            }
            else
            {
                _currentTreeResource = string.Empty;
            }

            _invoker.EnqueueCommandForExecution(_deselectAllCommand);
            _mode = (_currentTreeResource != string.Empty) ? PossibleModes.TreeDrawing : PossibleModes.None;
        }

        private void drawingPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (_mode == PossibleModes.TreeDrawing)
            {
                if (!string.IsNullOrWhiteSpace(_currentTreeResource))
                    _invoker.EnqueueCommandForExecution(_commandFactory.Create("add", _currentTreeResource, e.Location, _currentScale));
            }
            else if (_mode == PossibleModes.Selection && e.Button == MouseButtons.Left)
                _invoker.EnqueueCommandForExecution(_commandFactory.Create("select", e.Location));
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            LoadSaveForm dialog = new LoadSaveForm
            {
                Mode = LoadSaveForm.PossibleModes.Load,
                Response = _loadAndSaveResponse,
                UseAws = _loadAndSaveUseAws
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _loadAndSaveResponse = dialog.Response;
                _loadAndSaveUseAws = dialog.UseAws;
                _invoker.EnqueueCommandForExecution(_commandFactory.Create("load", Properties.Settings.Default.AwsBucket, _loadAndSaveResponse, _loadAndSaveUseAws));
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                DefaultExt = "json",
                RestoreDirectory = true,
                Filter = @"JSON files (*.json)|*.json|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _invoker.EnqueueCommandForExecution(_commandFactory.Create("save", dialog.FileName));
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            _invoker.EnqueueCommandForExecution(_removedSelectedCommand);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
        }

        private void DisplayDrawing()
        {
            if (_imageBuffer == null)
            {
                _imageBuffer = new Bitmap(drawingPanel.Width, drawingPanel.Height);
                _imageBufferGraphics = Graphics.FromImage(_imageBuffer);
                _panelGraphics = drawingPanel.CreateGraphics();
            }

            if (_drawing.Draw(_imageBufferGraphics))
                _panelGraphics.DrawImageUnscaled(_imageBuffer, 0, 0);
        }

        private void ComputeDrawingPanelSize()
        {
            int width = Width - drawingToolStrip.Width;
            int height = Height - fileToolStrip.Height;

            drawingPanel.Size = new Size(width, height);
            drawingPanel.Location = new Point(drawingToolStrip.Width, fileToolStrip.Height);

            _imageBuffer = null;
            if (_drawing != null)
                _drawing.IsDirty = true;
        }

        private void scale_Leave(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
            scale.Text = _currentScale.ToString(CultureInfo.InvariantCulture);
        }

        private float ConvertToFloat(string text, float min, float max, float defaultValue)
        {
            float result = defaultValue;
            if (!string.IsNullOrWhiteSpace(text))
                result = float.TryParse(text, out result) ? Math.Max(min, Math.Min(max, result)) : defaultValue;

            return result;
        }

        private void scale_TextChanged(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            _invoker.Undo();
        }

        private void redoButton_Click(object sender, EventArgs e)
        {
            _invoker.Redo();
        }
    }
}
