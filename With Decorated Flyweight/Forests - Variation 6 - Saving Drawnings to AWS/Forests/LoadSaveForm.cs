﻿using System;
using System.Windows.Forms;

namespace Forests
{
    public partial class LoadSaveForm : Form
    {
        public enum PossibleModes
        {
            Load,
            Save
        };

        private PossibleModes _mode;

        public PossibleModes Mode
        {
            get { return _mode; }
            set
            {
                _mode = value;
                Text = _mode == PossibleModes.Save ? @"Save" : @"Load";
            }
        }

        public bool UseAws
        {
            get { return _awsSelected.Checked; }
            set
            {
                _awsSelected.Checked = value;
            }
        }

        public string Response
        {
            get { return _fileOrKey.Text; }
            set { _fileOrKey.Text = value; }
        }

        public LoadSaveForm()
        {
            InitializeComponent();
        }

        private void awsSelected_CheckedChanged(object sender, EventArgs e)
        {
            AdjustLabels();
        }

        private void localFileSystemSelected_CheckedChanged(object sender, EventArgs e)
        {
            AdjustLabels();
        }

        private void AdjustLabels()
        {
            if (_awsSelected.Checked)
            {
                _fileOrKeyLabel.Text = @"AWS Key:";
                _browseButton.Enabled = false;
            }
            else
            {
                _fileOrKeyLabel.Text = @"Filename:";
                _browseButton.Enabled = false;
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            FileDialog dialog = (Mode == PossibleModes.Save) ? (FileDialog) new SaveFileDialog() : new OpenFileDialog();
            dialog.DefaultExt = ".json";

            dialog.InitialDirectory = "c:\\";
            dialog.Filter = @"JSON files (*.json)|*.json|All files (*.*)|*.*";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
                _fileOrKey.Text = dialog.FileName;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}
