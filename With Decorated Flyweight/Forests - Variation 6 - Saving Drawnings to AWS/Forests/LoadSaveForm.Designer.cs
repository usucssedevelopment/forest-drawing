﻿namespace Forests
{
    partial class LoadSaveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._destinationGroupBox = new System.Windows.Forms.GroupBox();
            this._awsSelected = new System.Windows.Forms.RadioButton();
            this._localFileSystemSelected = new System.Windows.Forms.RadioButton();
            this._fileOrKeyLabel = new System.Windows.Forms.Label();
            this._fileOrKey = new System.Windows.Forms.TextBox();
            this._browseButton = new System.Windows.Forms.Button();
            this._exitButton = new System.Windows.Forms.Button();
            this._cancelButton = new System.Windows.Forms.Button();
            this._destinationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _destinationGroupBox
            // 
            this._destinationGroupBox.Controls.Add(this._awsSelected);
            this._destinationGroupBox.Controls.Add(this._localFileSystemSelected);
            this._destinationGroupBox.Location = new System.Drawing.Point(33, 16);
            this._destinationGroupBox.Name = "_destinationGroupBox";
            this._destinationGroupBox.Size = new System.Drawing.Size(278, 53);
            this._destinationGroupBox.TabIndex = 0;
            this._destinationGroupBox.TabStop = false;
            this._destinationGroupBox.Text = "Destination";
            // 
            // _awsSelected
            // 
            this._awsSelected.AutoSize = true;
            this._awsSelected.Location = new System.Drawing.Point(170, 23);
            this._awsSelected.Name = "_awsSelected";
            this._awsSelected.Size = new System.Drawing.Size(66, 17);
            this._awsSelected.TabIndex = 1;
            this._awsSelected.Text = "AWS S3";
            this._awsSelected.UseVisualStyleBackColor = true;
            this._awsSelected.CheckedChanged += new System.EventHandler(this.awsSelected_CheckedChanged);
            // 
            // _localFileSystemSelected
            // 
            this._localFileSystemSelected.AutoSize = true;
            this._localFileSystemSelected.Checked = true;
            this._localFileSystemSelected.Location = new System.Drawing.Point(18, 23);
            this._localFileSystemSelected.Name = "_localFileSystemSelected";
            this._localFileSystemSelected.Size = new System.Drawing.Size(107, 17);
            this._localFileSystemSelected.TabIndex = 0;
            this._localFileSystemSelected.TabStop = true;
            this._localFileSystemSelected.Text = "Local File System";
            this._localFileSystemSelected.UseVisualStyleBackColor = true;
            this._localFileSystemSelected.CheckedChanged += new System.EventHandler(this.localFileSystemSelected_CheckedChanged);
            // 
            // _fileOrKeyLabel
            // 
            this._fileOrKeyLabel.AutoSize = true;
            this._fileOrKeyLabel.Location = new System.Drawing.Point(30, 94);
            this._fileOrKeyLabel.Name = "_fileOrKeyLabel";
            this._fileOrKeyLabel.Size = new System.Drawing.Size(52, 13);
            this._fileOrKeyLabel.TabIndex = 1;
            this._fileOrKeyLabel.Text = "Filename:";
            // 
            // _fileOrKey
            // 
            this._fileOrKey.Location = new System.Drawing.Point(117, 91);
            this._fileOrKey.Name = "_fileOrKey";
            this._fileOrKey.Size = new System.Drawing.Size(338, 20);
            this._fileOrKey.TabIndex = 2;
            // 
            // _browseButton
            // 
            this._browseButton.Location = new System.Drawing.Point(461, 89);
            this._browseButton.Name = "_browseButton";
            this._browseButton.Size = new System.Drawing.Size(34, 23);
            this._browseButton.TabIndex = 3;
            this._browseButton.Text = "...";
            this._browseButton.UseVisualStyleBackColor = true;
            this._browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // _exitButton
            // 
            this._exitButton.Location = new System.Drawing.Point(420, 132);
            this._exitButton.Name = "_exitButton";
            this._exitButton.Size = new System.Drawing.Size(75, 23);
            this._exitButton.TabIndex = 4;
            this._exitButton.Text = "Save";
            this._exitButton.UseVisualStyleBackColor = true;
            this._exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // _cancelButton
            // 
            this._cancelButton.Location = new System.Drawing.Point(33, 132);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 23);
            this._cancelButton.TabIndex = 5;
            this._cancelButton.Text = "Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            this._cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // LoadSaveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 176);
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._exitButton);
            this.Controls.Add(this._browseButton);
            this.Controls.Add(this._fileOrKey);
            this.Controls.Add(this._fileOrKeyLabel);
            this.Controls.Add(this._destinationGroupBox);
            this.Name = "LoadSaveForm";
            this.Text = "OpenForm";
            this._destinationGroupBox.ResumeLayout(false);
            this._destinationGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox _destinationGroupBox;
        private System.Windows.Forms.RadioButton _awsSelected;
        private System.Windows.Forms.RadioButton _localFileSystemSelected;
        private System.Windows.Forms.Label _fileOrKeyLabel;
        private System.Windows.Forms.TextBox _fileOrKey;
        private System.Windows.Forms.Button _browseButton;
        private System.Windows.Forms.Button _exitButton;
        private System.Windows.Forms.Button _cancelButton;
    }
}